# Quick Start

Install `yarn`.
Run `yarn` to install dependencies.

## Server

See the README under the `packages/server/` for information about how to get started on the server development.

## Client

See the README under the `packages/client/` for information about how to get started on the client development.
