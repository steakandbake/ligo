INTERNAL DOCUMENTATION OF THE SHARED PARSER FUNCTIONALITY

  Version.ml
    A source containing a commit hash. It should be deleted, as Dune
    knows how to generate and updated version.
  
  EvalOpt.mli
  EvalOpt.ml
    The module EvalOpt parses the command-line for options to the
    parser. That action is performed as a side-effect when the module
    is initialised at run-time: this is ugly and easy to fix. See
    ligo/src/parser/cameligo/EvalOpt.ml{i} for the right way to do
    it. Ignore them: the file actually calling directly the parser is
    ligo/src/parser/parser.ml. Note that, as a consequence, no option
    is currently passed to the parser when building Pascaligo with
    Dune. This should be made available.

  Markup.mli
  Markup.ml
    The definition of markup in source files, and some functions to 
    print or convert it to strings. You are unlikely going to modify 
    those files, as markup is pretty much the same for all LIGO 
    flavours.

  FQueue.mli
  FQueue.ml
    A naive implementation of purely functional queues. Replace by an
    imperative implementation if worst-case performance of single
    operations (queue/enqueue) is an issue.

  Error.mli
    The definition of the open type for errors: the lexer will add its
    own errors, the downside being that matching on errors requires a
    catch-all clause "| _ -> assert false" at the end. Note: the rest
    of the compiler uses an error monad.

  Lexer.mli
  Lexer.mll
    The Pascaligo lexer is generated from two ocamllex
    specifications. Lexer.mll is the first-level lexer. It exports a
    functor [Make] parameterised over a module [Token] defining the
    tokens, and returning a module whose signature is [Lexer.S]. (See
    Lexer.mli for a rationale.) If you write a new flavour of LIGO,
    this lexer is likely to be reused as is. Note that a great deal of
    the complexity of this lexer stems from its purpose to report
    stylistic errors (hence keeping temporarily scanned markup) and
    handling UTF-8 encoded comments. The first goal implies sometimes
    reading more than one token, and an extra-buffer has to be managed
    above the ocamllex one, so the parser is not confused about the
    location (region) of the token it has just read.

  LexerLog.ml
  LexerLog.mli
    Source for instantiating a standalone lexer for LexerMain.ml and
    ParserMain.ml. Ignore them.