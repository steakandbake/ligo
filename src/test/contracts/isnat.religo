let main = (i: int): option(nat) => Michelson.is_nat(i);
